<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table='question';
    protected $fillable=['question','image','category_id','user_id'];
    use HasFactory;
}
