## Final Project

## Kelompok 13

## Anggota Kelompok

- Iklima Mardiana (@iklimardiana)
- Berlian Fatimah Haryoko (@berlianfatimah)
- Idham Setia Budi (@kapiten-orca)

## Tema Project

Forum Diskusi

## ERD

<p align="center"><img src="public/img/ERD.jpeg"></p>

## Link Video Demo

-   Link Video Aplikasi : https://www.youtube.com/watch?v=mn5U34C8rKQ

## Link Deploy
-   Link Deploy : http://forumdiskusi.sanbercodeapp.com/

## Template

-  https://themewagon.github.io/PodTalk/?_ga=2.104269123.2106791418.1677145744-1964158481.1676687933
